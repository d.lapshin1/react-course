import React, { useEffect, useState } from 'react';
import axios, { AxiosError } from 'axios';
import { IProduct } from '../models';

export function useProducts() {
    const [products, setProducts] = useState<IProduct[]>([])
  const [loading, setloading] = useState(false)
  const [error, setError] = useState('')

  function addProduct(product: IProduct) {
    setProducts( prev => [...prev, product])
  }

  async function fetcProducts() {
    try {
    setError('')  
    setloading(true)
    const response = await axios.get<IProduct[]>('https://fakestoreapi.com/products')
    setProducts(response.data)
    setloading(false)
    } catch (e: unknown) {
      const error = e as AxiosError
      setloading(false)
      setError(error.message)

    }
  }

  useEffect(() => {
    fetcProducts()
  }, [])

    return { products, error, loading, addProduct}
}