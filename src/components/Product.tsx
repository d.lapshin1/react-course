import React, {useState} from 'react';
import { IProduct } from '../models';

interface ProductProps {
    product: IProduct
}

export function Product({ product }: ProductProps) {
    const [details, setDetails] = useState(false)

    const btnClassName = details ? 'bg-yellow-400' : 'bg-blue-400'

    const btnClasses = ['py-2 px-4 border bg-yellow-400', btnClassName]

    return (
        <div className='border py-2 px-4 rounded flex flex-col items-center mb-2'>
            <img src={product.image} className='w-1/4' alt={product.title} />
            <h3 className='mb-2'>{product.title}</h3>
            <span className='font-bold'>{product.price}</span>
            <button
                className={btnClasses.join(' ')}
                onClick={
                    () => setDetails(prev => !prev)
                }
            >{ details ? 'Hide' : 'Show' }</button>
            {
                details && <div>
                <p>{product.description}</p>
                <p>Rate: <span style={{fontWeight: 'bold'}}>{product?.rating?.rate}</span> </p>
            </div>
            }
        </div>
    )
}