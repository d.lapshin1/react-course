import React from 'react'

interface ErroMessageProps{
    error: string
    
}

export function ErrorMessage({error}: ErroMessageProps) {
    return (
        <p className='text-center text-red-600'>{ error }</p>
    )
}