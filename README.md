# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### React и TypeScript - Быстрый Курс [2022]

`yarn -v 1.21.1` \
`node -v 18.9.1` \
`react -v 18.2.0` 

Были проработы основные хуки `React`, а так же познакомился с работой `React` в связке с `TypeScript`.
